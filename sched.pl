%%
%%			PART 1
%%
c_numbers(N) :-
	course(N,_,_).

c_pl(N) :-
	course(N, programming_languages, _).

c_notpl(N) :-
	course(N, M, _),
	M \= programming_languages.

c_inst60(L) :-
	course(60, _, L).

c_inst60_sorted(L) :-
	course(60, _, L),
	sort(L).

c_inst20(L) :-
	course(20, _, L).

c_inst20_sorted(L) :-
	course(20, _, L),
	sort(L).

c_inst_sorted(N, L) :-
	course(N, _, L),
	sort(L).

c_single_inst(N) :-
	course(N, _, [_|[]]).

c_multi_inst(N) :-
	course(N, _, [_|T]),
	T \= [].

c_exclusive(I, N) :-
	course(N, _, [I|[]]).

c_12_inst_1or(N) :-
	course(N, _, [_|[]]);
	course(N, _, [_|[_|[]]]).

c_12_inst_2wo(N) :-
	course(N, _, [_|[]]).

c_12_inst_2wo(N) :-
	course(N, _, [_|[_|[]]]).

%%
%%			PART 2
%%

delete_question("157").

delete_test(_, [], []).
delete_test(X, [X|L], M) :- !, delete_test(X, L, M).
delete_test(X, [Y|L1], [Y|L2]) :- delete_test(X, L1, L2).

delete_test_2(X, [X|Y], Y).
delete_test_2(X, [Y|L1], [Y|L2]) :- delete_test_2(X, L1, L2).

sortappend(L1, L2, R) :-
	append(L1, L2, L12),
	sort(L12, R).

%%
%%			PART 3
%%

distribute(_, [], []).
distribute(W, X, Y) :-
	X = [H|T],
	distribute(W, T, S),
	append([[W,H]], S, Y).

%%
%%			PART 4
%%

myfor(L, U, Result) :-
	L =< U,
	L1 is L+1,
	myfor(L1, U, Res1),
	Result = [L | Res1].
myfor(L, U, []) :-
	L > U.


crossmyfor(R, H, Z) :-
	integer(R),
	integer(H),
	myfor(1, R, List1),
	myfor(1, H, List2),
	crossmyfor(List1, List2, Z).

crossmyfor([], _, []).
crossmyfor(L1, L2, R) :-
	L1 = [Head1 | Tail1],
	distribute(Head1, L2, X),
	crossmyfor(Tail1, L2, Y),
	append(X, Y, R).

%%
%%			PART 5a
%%

getallmeetings([], []).
getallmeetings(C, Z) :-
	C = [Head | Tail],
	Head = [_ | [Meetings]],
	getallmeetings(Tail, Y),
	sortappend(Meetings, Y, Z).

%%
%%			PART 5b
%%

participants([], []).
participants(C, Z) :-
	C = [[_ | [_]] | _],
	getallmeetings(C, AllM),
	participants(C, AllM, Z).

participants(_, [], []).
participants(Orig, [HMeets | TMeets], Result) :-
	attends(HMeets, Orig, People),
	singlemeeting(HMeets, People, Single),
	participants(Orig, TMeets, Y),
	sortappend([Single], Y, Result),!.

%Returns a list of people who attend Meeting
attends(_, [], []).
attends(Meeting, [[N | [M]] | TailNandM], Participants) :-
	member(Meeting, M),
	attends(Meeting, TailNandM, R),
	sortappend([N], R, Participants).
attends(Meeting, [[_ | [_]] | TailNandM], Participants) :-
	attends(Meeting, TailNandM,Participants).

singlemeeting(Meeting, Names, R) :-
	sortappend([Meeting], [Names], R).


%%
%%			PART 5c
%%

osched(_, _, [], []).
osched(MR, MH, C, Z) :-

	%logistics test
	getallmeetings(C, AllM),
	length(AllM, NumMeetings),
	NumMeetings =< MR * MH,

	%All meetings and their respective participants
	participants(C, P),

	%All combinations of rooms and times
	crossmyfor(MR, MH, RHCombos),

	permutation(P, PermP),

	once(oschedhelp(PermP, RHCombos, Z)).

oschedhelp([], [], []).
oschedhelp([], _, []).
oschedhelp(_, [], []).
oschedhelp([HeadP | TailP], [HeadRH | TailRH], Z) :-
	sortappend([HeadRH], [HeadP], Y),
	oschedhelp(TailP, TailRH, R),
	sortappend([Y], R, Z).

xsched(MR, MH, C, Z) :-

	%logistics test
	getallmeetings(C, AllM),
	length(AllM, NumMeetings),
	NumMeetings =< MR * MH,

	%All meetings and their respective participants
	participants(C, P),

	%All combinations of rooms and times
	crossmyfor(MR, MH, RHCombos),

	permutation(P, PermP),

	once(xschedhelp(PermP, RHCombos, Z)).

xschedhelp([], [], []).
xschedhelp([], _, []).
xschedhelp(_, [], []).
xschedhelp([HeadP | TailP], [HeadRH | TailRH], Z) :-
	sortappend([HeadRH], [HeadP], Y),
	HeadP = [Name | [_]],
	deleteparticipant(Name, [HeadP | TailP], [_|NewTailP]),!,
	xschedhelp(NewTailP, TailRH, R),
	sortappend([Y], R, Z).

deleteparticipant(_, [], []).
deleteparticipant(Name, P, Z) :-
	P = [[Meeting | [Names]] | CTail],
	member(Name, Names),
	Names = [_ | T],
	T \= [],
	delete(Names, Name, Nnew),!,
	Pnew = [[Meeting| [Nnew]] | CTail],
	deleteparticipant(Name, Pnew, Z).
deleteparticipant(Name, P, Z) :-
	P = [[Meeting | [Names]] | _],
	member(Name, Names),
	Names = [_|[]],
	delete(P, [Meeting | [Names]], Pnew),!,
	deleteparticipant(Name, Pnew, Z).
deleteparticipant(Name, P, Z) :-
	P = [Head | CTail],
	deleteparticipant(Name, CTail, R),
	append([Head], R, Z).